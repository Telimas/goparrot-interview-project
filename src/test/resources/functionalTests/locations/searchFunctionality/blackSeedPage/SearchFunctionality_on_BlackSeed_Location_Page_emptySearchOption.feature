Feature: Search Functionality on Black Seed location page - empty search option
  Description: This feature will handle scenario with empty option for search field functionality on BlackSeed page.

  Scenario: Navigating to BlackSeed location page
    Given I am on BlackSeed location page

  Scenario: Search field is displayed
    Given I see search field

  Scenario: Using empty option inside search field
    Given I clear and insert " " into search field
    Then I do not see search dropdown
    When I click find button
    #It's just for demonstration
    And I wait 3 seconds
    And I see validation message
    #It's just for demonstration
    And I wait 3 seconds

  Scenario: Test CleanUp: Closing browser
    Given I close browser