# README #

### What is this repository for? ###

* It's a part of GoParrot technical interview for QA Automation Engineer position. 
* Version 1.0

### How do I get set up? ###

* You should have Java 8 or higher installed.
* Clone code as a maven project.

### Contribution guidelines ###

* Contains 3 feature files.