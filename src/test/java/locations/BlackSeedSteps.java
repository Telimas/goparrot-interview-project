package locations;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import pageObjects.locations.BlackSeedPage;

import java.util.concurrent.TimeUnit;

public class BlackSeedSteps {
    private final static String PAGE_URL = "https://order.blackseedbagels.com/locations/blackseedbagels";

    static WebDriver driver;
    static BlackSeedPage blackSeedPage;

    @Given("I am on BlackSeed location page")
    public void iAmOnBlackSeedLocationPage() {
        System.setProperty("webdriver.chrome.driver", "driver\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.get(PAGE_URL);
        driver.switchTo().frame(0);
        blackSeedPage = new BlackSeedPage(driver);
        blackSeedPage.waitForPageLoaded();
    }

    @Given("I see search field")
    public void iSeeSearchField() {
        blackSeedPage = new BlackSeedPage(driver);
        if (!blackSeedPage.searchField_isDisplayed())
            throw new ElementNotVisibleException("Search field is not displayed");
    }

    @Given("I clear and insert {string} into search field")
    public void iClearAndInsertIntoSearchField(String text) {
        blackSeedPage.searchField_ClearAndSendKeys(text);
    }

    @Then("I do see search dropdown")
    public void iSeeSearchDropdown() {
        if (!blackSeedPage.searchDropdown_isDisplayed())
            throw new ElementNotVisibleException("Search dropdown is not displayed");
    }

    @Then("I do not see search dropdown")
    public void iDoNotSeeSearchDropdown() {
        if (blackSeedPage.searchDropdown_isDisplayed())
            throw new RuntimeException("Search dropdown is displayed");
    }

    @And("I should see {string} in search dropdown")
    public void iShouldSeeInSearchDropdown(String text) {
        blackSeedPage = new BlackSeedPage(driver);
        if (!blackSeedPage.isOptionInside_SearchResults(text))
            throw new NoSuchElementException(String.format("No such option [%s] inside search results dropdown", text));
    }

    @Given("I close browser")
    public void iCloseBrowser() {
        driver.close();
        driver.quit();
    }

    @And("I see validation message")
    public void iSeeValidationMessage() {
        blackSeedPage = new BlackSeedPage(driver);
        if (!blackSeedPage.validation_searchField_isDisplayed())
            throw new ElementNotVisibleException("Validation message is not displayed");
    }

    @When("I click find button")
    public void iClickFindButton() {
        blackSeedPage.click_button_find();
    }

    @And("I wait {int} seconds")
    public void iWaitSeconds(int sec) throws InterruptedException {
        Thread.sleep(sec * 1000);
    }
}
