Feature: Search Functionality on Black Seed location page - positive scenarios
  Description: This feature will handle positive scenarios for search field functionality on BlackSeed page.

  Scenario: Navigating to BlackSeed location page
    Given I am on BlackSeed location page

  Scenario: Search field is displayed
    Given I see search field

  Scenario Outline: Using correct [<location>] inside search field
    Given I clear and insert "<text>" into search field
    #It's just for demonstration
    And I wait 2 seconds
    Then I do see search dropdown
    And I should see "<text>" in search dropdown

    Examples:
      | location | text          |
      | city     | Jersey City   |
      | state    | Massachusetts |
      | zip      | 10036         |

  Scenario: Test CleanUp: Closing browser
    Given I close browser