Feature: Search Functionality on Black Seed location page - negative scenarios
  Description: This feature will handle negative scenarios for search field functionality on BlackSeed page.

  Scenario: Navigating to BlackSeed location page
    Given I am on BlackSeed location page

  Scenario: Search field is displayed
    Given I see search field

  Scenario Outline: Using wrong [<location>] inside search field
    Given I clear and insert "<text>" into search field
    #It's just for demonstration
    And I wait 2 seconds
    Then I do not see search dropdown

    Examples:
      | location   | text         |
      | characters | testingCity  |
      | numbers    | NY 100361234 |
      | symbols    | !@#$%^&*()   |

  Scenario: Test CleanUp: Closing browser
    Given I close browser