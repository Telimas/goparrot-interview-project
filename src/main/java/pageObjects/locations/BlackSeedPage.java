package pageObjects.locations;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import pageObjects.AbstractPage;

import java.util.List;

public class BlackSeedPage extends AbstractPage {

    public BlackSeedPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(how = How.XPATH, using = "//*[@id='searchBox']")
    private WebElement textField_searchField;

    public boolean searchField_isDisplayed() {
        return textField_searchField.isDisplayed();
    }

    public void searchField_ClearAndSendKeys(String text) {
        textField_searchField.clear();
        try {
            sendKeysSlowly(textField_searchField, text);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @FindBy(how = How.XPATH, using = "//*[@class='pac-container pac-logo']")
    private WebElement dropdown_search;

    public boolean searchDropdown_isDisplayed() {
        return dropdown_search.isDisplayed();
    }

    @FindAll(@FindBy(how = How.XPATH, using = "//*[@class='pac-container pac-logo']//*[@class='pac-item']//*[@class='pac-matched']"))
    private List<WebElement> dropdown_searchResults;

    public boolean isOptionInside_SearchResults(String text) {
        for (WebElement element : dropdown_searchResults) {
            if (element.getAttribute("textContent").equals(text))
                return true;
        }
        return false;
    }

    @FindBy(how = How.XPATH, using = "//*[@class='searchbox-help' and text()='Please be more specific']")
    private WebElement validation_searchField;

    public boolean validation_searchField_isDisplayed() {
        return validation_searchField.isDisplayed();
    }

    @FindBy(how = How.XPATH, using = "//*[@class='sc-VigVT fIDHGX']")
    private WebElement button_find;

    public void click_button_find() {
        button_find.click();
    }
}
